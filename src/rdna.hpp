#include <string>
#include <random>

using std::string;

string randDNA(int s, string b, int n)
{
	int i;
	int max = b.length ()-1;
	
	std::string ATCG = "";
	
	std::mt19937 eng(s);
	std::uniform_int_distribution<>uniform(0,max);
	
	for (i = 0; i < n; i++)
	{
		ATCG += b[uniform(eng)];
	}
	
return ATCG;
}
